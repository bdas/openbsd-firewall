# Let's start with a FAQ...

## What is this ?
   A set of roles to make it easy to deploy an openbsd based firewall and router.

## You have made unsafe choices, your firewall policy is very risky.
   Probably, but it works well for my needs. Things could change later.

## Features X and Y are missing from the roles.
   Same as above. But ofcourse, if something is generally useful, please send a MR.

## Why OpenBSD ?
   It's reminiscent of the original days. The man page is excellent and it still preserves
   the philosophy of a simple configuration mechanism via text files. That said, we do install
   some packages that do not come in the original install.

## Did you write all the roles ?
   Some roles are from https://github.com/liv-io/ansible-roles-bsd
   I initially intended to just use them as submodules but my local changes were just too much
   to keep track of.

## Do you have an example of how to set things up ?
   Please take a look at setup.yml which implements a basic firewall and router. It assumes two
   network interfaces but you could might as well just repurpose it with one interface and use vlans
   for everything. 

(More later...)



